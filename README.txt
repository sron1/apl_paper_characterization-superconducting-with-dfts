Reproduction package for "Characterization of superconducting NbTiN films using a Dispersive Fourier Transform Spectrometer"

This is the reproduction package for the paper "Characterization of superconducting NbTiN films using a Dispersive Fourier Transform Spectrometer" by Lap et al. (2021).

Please note that this is a reproduction package created about 4 years after the publication. It aims to provide the most important data products and script for reproducing the results of the paper. 
It is, however, not fully complete, especially some fo the Nb 200nm scripts and data are incomplete 

=== Content ===
The packages contains two main folder, \data and \figures.
\data holds the underlying scripts and data for the constrast fitting, and is divided into \fit_Nb and \fit_NbTiN. These contain
the relevent scripts and measurment data for the corresponding films. Each subfolder contains a README.txt explaining the contents.
 
\figures holds the scripts for producing the figures of the paper, each subfolder subfolder contains a README.txt explaining the contents
and the procedure for obtaining the corresponding figure.

=== Software prerequisites ===
To run the scripts in this package, the following software is recommended:
- Python 2.7
- Inkscape (or any software able to handle .svg files)

=== Software ====
The following .py files are data analysis scripts:
- fit_Nb70nm.py
- fit_NbTiN_si_solid.py
- fit_NbTiN_si_film.py
- fit_NbTiN_qu_film.py

The following .py files are (modified) Mattis Bardeen theory packages: 
- Nb_modified_MB.py
- NbTiN_MB.py
- NbTiN_modified_MB.py

The following .py files are plotting script: 
- nbtin_spectrum_paper.py
- Nb_plotting.py
- NbTiN_plotting.py