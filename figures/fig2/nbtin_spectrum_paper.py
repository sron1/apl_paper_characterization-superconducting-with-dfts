#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt 
import matplotlib.collections as mcol
import matplotlib.transforms as mtransforms
from matplotlib.legend_handler import HandlerPathCollection
from matplotlib import cm
import numpy as np 
from scipy import optimize
import NbTiN_fit_normal as n
import NbTiN_fit_broad as b
import time
from mpl_toolkits.mplot3d import Axes3D
from scipy import interpolate

f_con_si    = np.loadtxt('./conductivity_fit_nbtin_si.txt')[:,0]
real_6k     = np.loadtxt('./conductivity_fit_nbtin_si.txt')[:,1]
imag_6k     = np.loadtxt('./conductivity_fit_nbtin_si.txt')[:,2]
real_15k    = np.loadtxt('./conductivity_fit_nbtin_si.txt')[:,3]
imag_15k    = np.loadtxt('./conductivity_fit_nbtin_si.txt')[:,4]

f_cont_si   = np.loadtxt('./contrast_fit_nbtin_si.txt')[:,0]
cont_si_MB  = np.loadtxt('./contrast_fit_nbtin_si.txt')[:,2]
cont_si     = np.loadtxt('./contrast_fit_nbtin_si.txt')[:,1]

f_cont_si_dat = np.loadtxt('./contrast_data_nbtin_si.txt')[:,0]
cont_si_dat = np.loadtxt('./contrast_data_nbtin_si.txt')[:,1]


#Fomulas
class HandlerMultiPathCollection(HandlerPathCollection):
    """
    Handler for PathCollections, which are used by scatter
    """
    def create_collection(self, orig_handle, sizes, offsets, transOffset):
        p = type(orig_handle)(orig_handle.get_paths(), sizes=sizes,
                              offsets=offsets,
                              transOffset=transOffset,
                              )
        return p
# Selecting the data 
def selector(temp, index, N):
    for i in range(len(temp)):
        count   = 0
        avg_y   = 0
        s_avgcom= 0
        s_avg   = 0
        fft_avg = 0

        avg_y2  = 0
        s_avg2  = 0 
        fft_avg2= 0

        while count < 3 :
            y       = np.loadtxt('./dat/NbTiN200nm_'+str( temp[i] + count )+'.dat')[:,1]
            a, b    = np.polyfit(pos, y, 1)
            y_new   = a * pos + b
            y_temp  = y - y_new
            avg_y   += y - y_new

            y_temp2 = y_temp[200:]
            avg_y2  += y_temp2

            s       = np.loadtxt('./fts/NbTiN200nm_'+str( temp[i] + count )+'.fts')[:,1]

            X       = np.roll(y_temp, N)
            fft     = np.fft.fftshift( np.fft.fft( X ) )[ N : ]
            imag    = np.imag(fft)
            real    = np.real(fft)
            s_avg   += np.sqrt( (real**2 + imag**2) / ( (N)*2) ) 
            temp_s  = np.sqrt( (real**2 + imag**2) / ( (N)*2) ) 
            fft_avg += fft

            N2      = len(y_temp2)/2 - 1
            fft2    = np.fft.fftshift( np.fft.fft( y_temp2 ) )[ N2 : ]
            imag2   = np.imag(fft2)
            real2   = np.real(fft2)
            s_avg2  += np.sqrt( (real2**2 + imag2**2) / ( (N2)*2) ) 
            temp_s2 = np.sqrt( (real2**2 + imag2**2) / ( N2 *2 ) ) 
            fft_avg2+= fft2

            # print "len(s): ", len(s), "len temp_s: ", len(temp_s), " N: ", N
            count   += 1

        if index == 1:
            dat_c[i]= avg_y / 3.
            spe_c[i]= s_avg / 3.
            fft_c[i]= fft_avg / 3.

            ratio   = np.sum(spe_c[i]) / np.sum( s_avg2 / 3.)
            dat_c_s[i]= avg_y2 / 3.
            spe_c_s[i] = s_avg2 / 3. / ratio
            fft_c_s[i] = fft_avg2 / 3.

        if index == 2:
            dat_w[i]= avg_y / 3.
            spe_w[i]= s_avg / 3.
            fft_w[i]= fft_avg / 3.

            ratio   = np.sum(spe_w[i]) / np.sum( s_avg2 / 3.)
            dat_w_s[i]= avg_y2 / 3.
            spe_w_s[i]= s_avg2 / 3. / ratio
            fft_w_s[i]= fft_avg2 / 3.

# Contrast calculation
def cont_cal(spe_w, len_w, spe_c, len_c, f, N, len_T):
    S       = np.zeros( (len_c , len(f) ) )     # = Each contrast line
    Sf      = np.zeros( (len_T , len(f) ) )   # = Average contrast line per temperature point
    St      = 0 
    sel     = [0,1]
    Nc      = 0 

    for j in range(len_w):
        for k in sel:
            z   = j * 2 + k
            # print 'z: %-5s j: %s' % ( z, j)
            S[z]= ( spe_c[z] - spe_w[j] ) / spe_w[j]
            St  += S[z]
            Nc  += 1

            q       = j / N
            
            if Nc == N*2:
                Sf[q]   = St / float(N*2)
                # print     "T:", T[q] , " is stored \n"
                St      = 0  
                Nc      = 0

    return Sf, S

# Script
# Preliminaries
# T       = [7., 9., 11., 13., 5.3, 6.]#, 13.]
# # Measurement #10 NbTiN
# start   = 1358
# end     = 1432

# # Measurement #9 NbTiN
# start2   = 1214
# end2     = 1357

T       = [6., 7., 9., 11., 13., ]#, 13.]
start   = 1873
end     = 2052

step    = 9
step2   = 3
N       = 4
meas1   = range(start, end+1)

temp1   = range(start+step2, end, step)
# temp2   = range(start2+step2, end2, step)
temp_w  = temp1
temp_c  = []
for i in temp_w:
    temp_c.append(i-step2)
    temp_c.append(i+step2)

# print temp_c

# Data arrays
pos     = np.loadtxt('./dat/NbTiN200nm_'+str(start)+'.dat')[:,0]
dat_w   = np.zeros( ( len(temp_w) , len(pos) ) )
dat_c   = np.zeros( ( len(temp_c) , len(pos) ) )

f       = np.loadtxt('./fts/NbTiN200nm_'+str(start)+'.fts')[:,0]
spe_w   = np.zeros( ( len(temp_w) , len(f) ) )
spe_c   = np.zeros( ( len(temp_c) , len(f) ) )
fft_w   = np.zeros( ( len(temp_w) , len(f) ), dtype=complex )
fft_c   = np.zeros( ( len(temp_c) , len(f) ), dtype=complex )


pos_s   = pos[200:]
dat_w_s = np.zeros( ( len(temp_w) , len(pos_s) ) )
dat_c_s = np.zeros( ( len(temp_c) , len(pos_s) ) )
spe_w_s = np.zeros( ( len(temp_w) , 152 ) )
spe_c_s = np.zeros( ( len(temp_c) , 152 ) )
fft_w_s = np.zeros( ( len(temp_w) , 152 ), dtype=complex )
fft_c_s = np.zeros( ( len(temp_c) , 152 ), dtype=complex )

selector(temp_c, 1, len(f) - 1 )
selector(temp_w, 2, len(f) - 1 )
Sf, S   = cont_cal(spe_w, len(temp_w), spe_c, len(temp_c), f, N, len(T) )

pos_step  = 20 * 10**(-6)
t         = pos_step / (3 * 10**(8) )
freq_step = 1 / ( 152 * t ) 

f_s = np.arange( 0, 152 ) * freq_step  * .5 * .5 
Sf_s, S_s   = cont_cal(spe_w_s, len(temp_w), spe_c_s, len(temp_c), f_s, N, len(T) )

spe_c_nb    = np.loadtxt('./spec_w_c_nb.txt')[:,1]
spe_w_nb    = np.loadtxt('./spec_w_c_nb.txt')[:,2]
f_nb        = np.loadtxt('./spec_w_c_nb.txt')[:,0]

plt.rcParams.update({'font.size': 40, 'legend.fontsize': 45})
plt.rcParams['axes.linewidth'] = 4

from matplotlib.font_manager import FontProperties
font = FontProperties()

fig = plt.figure(1)
ax  = fig.add_subplot(111)
font.set_weight('bold')
fig.set_size_inches(20, 15)
plt.title("Reflection spectrum NbTiN")
plt.plot(f_s/1e12, 1000*spe_c_s[0], linewidth=6,color=".5", label="Cold NbTiN")
plt.plot(f_s/1e12, 1000*spe_w_s[0], linewidth=6,color="k", label="Hot NbTiN")

plt.hlines(.22, min(f_s/1e12), max(f_s/1e12), linewidth=6, linestyle="dashed" ,color='.4')

plt.xlabel("Frequency (THz)")
plt.ylabel("Spectral weight")

plt.yticks(np.arange(0,5.+.5,.5 ))
plt.ylim((0,5))
plt.xticks(np.arange(0,4.+.5,.5 ))
plt.xlim((0,3))
ax.tick_params(direction='in', length=15, width=5 , which='major', pad=15)
# ax.xaxis.set_label_coords(0, .1)

plt.legend(bbox_to_anchor=(.75, 0.15, .25, .17))

a = plt.axes([.6, .4, .27, .45])
a.tick_params(direction='in', length=15, width=5 , which='major', pad=15 )

plt.plot(f_s/1e12, 1000*spe_c_s[0],color=".5",  linewidth=6)
plt.plot(f_s/1e12, 1000*spe_w_s[0],color="k",    linewidth=6)

# plt.xlabel("Frequency (THz)")
# plt.ylabel("Spectral weight")
plt.yticks(np.arange(1,5+1,.5 ))
plt.xticks(np.arange(0,4+.5,.5 ))
plt.ylim(2.5,4.6)
# plt.grid(True,linewidth=2)
plt.xlim(0.5, 1.5)

plt.savefig("./NbTiN_fit_paper/nbtin_spectrum.png")
plt.show()