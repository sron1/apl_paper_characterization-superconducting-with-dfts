Figure 1 from apl_paper_characterization-superconducting-with-dfts was created using measured NbTiN film spectra and python.

Here, .dat are the interferograms and .fts are the corresponding spectra. A selected range of measurements is taken to improve the repesentation
of the spectra. 

These are used to the nbtin_spectrum_paper.py, which was used to create the figure.