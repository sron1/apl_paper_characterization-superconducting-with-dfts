#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

# COntrast data
R_fit_Nb 	= np.loadtxt('contrast_fit_nb70.txt')
R_dat_Nb	= np.loadtxt('contrast_data_nb70.txt')

# Conductivity data
con_fit_Nb  = np.loadtxt('conductivity_fit_Nb70.txt')


R4k200 		= np.load('R_4k_200nm.npy')
R6k200 		= np.load('R_6k_200nm.npy')
Rsim200 	= np.load('R_sim_200nm.npy')

f_clean2 	= np.load('f_clean_200nm.npy')
freq_s2 	= np.load('freq_s_200nm.npy')

# Conductivity data
con200 		= np.load('Con_200nm.npy')

plt.rcParams.update({'font.size': 25, 'legend.fontsize': 20})
plt.rcParams['axes.linewidth'] = 4

from matplotlib.font_manager import FontProperties
font = FontProperties()

f, axarr = plt.subplots(2, sharex=True)
f.set_size_inches(15, 15)

# PLot contrast
# 70nm
# T = 4.7
axarr[0].plot(R_fit_Nb[:,0], R_fit_Nb[:,1], linewidth=7, color="b", label="70nm Theory 4.7K ")
axarr[0].plot(R_dat_Nb[:,0], 100*R_dat_Nb[:,1], color='b', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="70nm Data 4.7K ")
# T = 6 k
axarr[0].plot(R_fit_Nb[:,0], R_fit_Nb[:,2], linewidth=7, color="r", label="70nm Theory 6K ")
axarr[0].plot(R_dat_Nb[:,0], 100*R_dat_Nb[:,2], color='r', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="70nm Data 6K ")

# 200nm
axarr[0].plot(freq_s2/1e12, 100*(Rsim200[0]-Rsim200[2])/Rsim200[2], linewidth=7, color="g", label="200nm MB 4.5K ")
axarr[0].plot(freq_s2/1e12, 100*(Rsim200[1]-Rsim200[2])/Rsim200[2], linewidth=7, color="y", label="200nm Model 6K ")
axarr[0].plot( f_clean2/1e12 , 100*R4k200  ,color='g', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="200nm Data 4.5K ")
axarr[0].plot( f_clean2/1e12 , 100*R6k200  ,color='y', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="200nm Data 6.0K ")
# Horizontal line
axarr[0].hlines(y=0, xmin=0.0, xmax=np.max(1000*R_fit_Nb[:,0]), color='k')

# Layout subplot
axarr[0].set_xticks(np.arange(0,1.8+.300,.3 ))
axarr[0].set_xlim((0, 1.8))
axarr[0].tick_params(width=10)
axarr[0].set_yticks(np.arange(-2.5,3.5,1))
axarr[0].set_ylim((-3, 3))
axarr[0].set_ylabel("Reflection contrast (%)")
axarr[0].legend(ncol=2,fontsize=17.5)

# Plot Conductivity
axarr[1].plot(con_fit_Nb[:,0]/1000, con_fit_Nb[:,1]*10, linewidth=10, color="b", label="70nm Real 4.7K ", alpha=0.7)
axarr[1].plot(con_fit_Nb[:,0]/1000, con_fit_Nb[:,3]*10, linewidth=10, color="r", label="70nm Real 6K", alpha=0.7)
axarr[1].plot(con_fit_Nb[:,0]/1000, con_fit_Nb[:,5]*10, linewidth=10, color="g", label="70nm Real 10K", alpha=0.7)

axarr[1].plot(con_fit_Nb[:,0]/1000, con_fit_Nb[:,2]*10, color="b", linewidth=10, linestyle=':', label="70nm Imag 4.7K")
axarr[1].plot(con_fit_Nb[:,0]/1000, con_fit_Nb[:,4]*10, color="r", linewidth=10, linestyle=":", label="70nm Imag 6K")
axarr[1].plot(con_fit_Nb[:,0]/1000, con_fit_Nb[:,6]*10, color="g", linewidth=10, linestyle=":", label="70nm Imag 10K")

axarr[1].set_yticks(np.arange(0,3.5+.5,.5))
axarr[1].set_ylim((-0.01,2.5))
axarr[1].set_xlabel("Frequency (THz)")
axarr[1].set_ylabel("$\Omega$ (Sm/m x $10^7$)")
axarr[1].legend(ncol=2,fontsize=17.5)

f.subplots_adjust(hspace=0)
for ax in axarr:
    ax.label_outer()
    ax.tick_params(width=5, direction='in', length=10)

plt.savefig("./Nb_plot_paper.png")
plt.show()


from matplotlib.font_manager import FontProperties
font = FontProperties()

f, axarr = plt.subplots(1, sharex=True)
f.set_size_inches(15, 7.5)

# T = 4.7
axarr.plot(R_fit_Nb[:,0], R_fit_Nb[:,1], linewidth=7, color="r", label="70nm Theory 4.7K ")
axarr.plot(R_dat_Nb[:,0], 100*R_dat_Nb[:,1], color='r', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="70nm Data 4.7K ")

# T = 6 k
axarr.plot(R_fit_Nb[:,0], R_fit_Nb[:,2], linewidth=7, color="g", label="70nm Theory 6K ")
axarr.plot(R_dat_Nb[:,0], 100*R_dat_Nb[:,2], color='g', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="70nm Data 6K ")

# Con_200nm
axarr.plot(freq_s2/1e12, 100*(Rsim200[0]-Rsim200[2])/Rsim200[2], linewidth=7, color="b", label="200nm MB 4.5K ")
axarr.plot(freq_s2/1e12, 100*(Rsim200[1]-Rsim200[2])/Rsim200[2], linewidth=7, color="c", label="200nm Model 6K ")
axarr.plot( f_clean2/1e12 , 100*R4k200  ,color='b', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="200nm Data 4.5K ")
axarr.plot( f_clean2/1e12 , 100*R6k200  ,color='c', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="200nm Data 6.0K ")

# Horizontal line
axarr.hlines(y=0, xmin=0.0, xmax=np.max(1000*R_fit_Nb[:,0]), color='k', linestyle='--')

# Layout subplot
axarr.set_xticks(np.arange(0,1.8+.300,.3 ))
axarr.set_xlim((0, 1.8))
axarr.tick_params(size=15)
axarr.set_yticks(np.arange(-2.5,3.5,1))
axarr.grid(True)
axarr.set_ylim((-3, 3))
axarr.set_ylabel("Contrast in Reflection (%)")
axarr.set_xlabel("Frequency (THz)")
axarr.legend(ncol=2,fontsize=17.5)

plt.savefig("./fig3.png")
plt.show()
