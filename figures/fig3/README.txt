Nb film contrast data (70nm and 200nm), fitted contrast (70nm and 200nm) and obtained conductivity (70nm only) are plotted using
Nb_plotting.py to create fig3.png 

Used inputs:
- text files in \data\fit_Nb\70nm\output
- text files in \data\fit_Nb\200nm\output