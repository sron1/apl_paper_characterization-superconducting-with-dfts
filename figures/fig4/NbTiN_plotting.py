#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

'''
Load data
'''
# COntrast data
R_fit_NbTiN_qu 	= np.loadtxt('contrast_fit_nbtin_qu_paper.txt')
R_dat_NbTiN_qu 	= np.loadtxt('contrast_data_nbtin_qu_paper.txt')

R_fit_NbTiN_si 	= np.loadtxt('contrast_fit_nbtin_si_paper.txt')
R_fit_NbTiN_si2 = np.loadtxt('contrast_NbTiN_mesh_new.dat')
R_dat_NbTiN_si 	= np.loadtxt('contrast_data_nbtin_si_paper.txt')

R_fit_NbTiN_solid_si    = np.loadtxt('contrast_fit_nbtin_solid_si_paper.txt')
R_dat_NbTiN_solid_si    = np.loadtxt('contrast_data_nbtin_solid_si_paper.txt')

# Conductivity data
con_fit_NbTiN_qu        = np.loadtxt('conductivity_fit_nbtin_qu_paper.txt')
con_fit_NbTiN_si        = np.loadtxt('conductivity_fit_nbtin_si_paper.txt')
con_fit_NbTiN_si_solid 	= np.loadtxt('conductivity_fit_nbtin_solid_si_paper.txt')

'''
Plotting
'''
plt.rcParams.update({'font.size': 25, 'legend.fontsize': 20})
plt.rcParams['axes.linewidth'] = 4

from matplotlib.font_manager import FontProperties
font = FontProperties()

f, axarr = plt.subplots(2, sharex=True)
f.set_size_inches(15, 15)

## Plot contrast
# Solid Si
axarr[0].plot(R_fit_NbTiN_solid_si[:,0]/1000, R_fit_NbTiN_solid_si[:,1], linewidth=7, color="k", label="Solid Si/Quartz Theory 5.4K ")
axarr[0].plot(R_dat_NbTiN_solid_si[:,0], R_dat_NbTiN_solid_si[:,1], color='k', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="Solid Si/Quartz Data 5.4K ")

# Qu film
axarr[0].plot(R_fit_NbTiN_qu[:,0]/1000, R_fit_NbTiN_qu[:,1], linewidth=7, color="b", label="Quartz Theory 6.0K ")
axarr[0].plot(R_dat_NbTiN_qu[:,0], R_dat_NbTiN_qu[:,1], color='b', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="Quartz Data 6.0K ")

# Si film
axarr[0].plot(R_fit_NbTiN_si2[:,0]/1000, 100* R_fit_NbTiN_si2[:,1], linewidth=7, color="r", label="Si Theory 4.6K ")
axarr[0].plot(R_dat_NbTiN_si[:,0]/1000, 100*R_dat_NbTiN_si[:,1], color='r', marker=".", markersize=20, markerfacecolor='none',markeredgewidth=4, linestyle="", label="Si Data 4.6K ")

# Horizontal line
axarr[0].hlines(y=0, xmin=0.0, xmax=np.max(R_fit_NbTiN_si[:,0]/1000), color='k')

# Layout subplot
axarr[0].set_xticks(np.arange(0,3+.300,.300 ))
axarr[0].set_xlim((0, 3))
axarr[0].tick_params(width=10)
axarr[0].set_yticks(np.arange(-5,7.5+2.5,2.5))
axarr[0].set_ylabel("Reflection contrast (%)")
axarr[0].legend(ncol=2,fontsize=17.5)

# Plot Conductivity
# Qu
axarr[1].plot(con_fit_NbTiN_qu[:,0]/1000, con_fit_NbTiN_qu[:,1], linewidth=10, color="b", label="Quartz Real 6.0K ", alpha=0.7)
axarr[1].plot(con_fit_NbTiN_qu[:,0]/1000, con_fit_NbTiN_qu[:,3], linewidth=10, color="r", label="Quartz Real 16.0K", alpha=0.7)
axarr[1].plot(con_fit_NbTiN_qu[:,0]/1000, con_fit_NbTiN_qu[:,2], color="b", linewidth=10, linestyle='-.', label="Quartz Imag 6.0K")
axarr[1].plot(con_fit_NbTiN_qu[:,0]/1000, con_fit_NbTiN_qu[:,4], color="r", linewidth=10, linestyle="-.", label="Quartz Imag 16.0K")
# Si
axarr[1].plot(con_fit_NbTiN_si[:,0]/1000, con_fit_NbTiN_si[:,1], linewidth=7, linestyle='--',color="g", label="Si Real 4.6K ", alpha=0.7)
axarr[1].plot(con_fit_NbTiN_si[:,0]/1000, con_fit_NbTiN_si[:,3], linewidth=7, linestyle='--',color="y", label="Si Real 16.0K", alpha=0.7)
axarr[1].plot(con_fit_NbTiN_si[:,0]/1000, con_fit_NbTiN_si[:,2], color="g", linewidth=7, linestyle=':', label="Si Imag 4.6K")
axarr[1].plot(con_fit_NbTiN_si[:,0]/1000, con_fit_NbTiN_si[:,4], color="y", linewidth=7, linestyle=":", label="Si Imag 16.0K")

axarr[1].set_yticks(np.arange(0,.35+.05,.05))
axarr[1].set_ylim((-0.01,.25))
axarr[1].set_xlabel("Frequency (THz)")
axarr[1].set_ylabel("$\Omega$ (Sm/m x $10^7$)")
axarr[1].legend(ncol=2,fontsize=17.5)

f.subplots_adjust(hspace=0)
for ax in axarr:
    ax.label_outer()
    ax.tick_params(width=5, direction='in', length=10)

plt.savefig("./fig4.png")
plt.show()
