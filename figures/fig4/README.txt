NbTiN Si and Qu film contrast data , fitted contrast and obtained conductivities are plotted using NbTiN_plotting.py to create fig4.png 

Used inputs:
- text files in \data\fit_NbTiN\solid\output
- text files in \data\fit_NbTiN\film\Si\output
- text files in \data\fit_NbTiN\film\Qu\output