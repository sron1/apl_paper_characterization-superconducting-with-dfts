#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
This python script describes the fitting of the constrast curve of the NbTiN Si film by modified Mattis Bardeen (MB) theory.

The outline of the script is as follows:
Step 0: Preliminaries
    - Load packages
    - define functions

Step 1: Load data
    - Preliminaries
    - load measurment data
    - load 4D data cube

Step 2: fitting and storage
    - Preliminaries
    - execute fitting procedure

Step 3: Plotting and saving
'''

'''
Step 0.1: Preliminaries / load packages
'''
# Plotting packages
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
# Numeric packages
import numpy as np
from scipy import optimize
from scipy import interpolate
# Modified Mattis-Bardeen (MB) package
import NbTiN_modified_MB as n
# Set directory
dir_name    = 'complex4_verified' ;

'''
Step 0.2: Preliminaries / Functions
'''
def selector(temp, index, N):
    for i in range(len(temp)):
        count   = 0
        avg_y   = 0
        s_avgcom= 0
        s_avg   = 0
        fft_avg = 0

        avg_y2  = 0
        s_avg2  = 0
        fft_avg2= 0

        while count < 3 :
            y       = np.loadtxt('./dat/NbTiN200nm_'+str( temp[i] + count )+'.dat')[:,1]
            a, b    = np.polyfit(pos, y, 1)
            y_new   = a * pos + b
            y_temp  = y - y_new
            avg_y   += y - y_new

            y_temp2 = y_temp[200:]
            avg_y2  += y_temp2

            s       = np.loadtxt('./fts/NbTiN200nm_'+str( temp[i] + count )+'.fts')[:,1]

            X       = np.roll(y_temp, N)
            fft     = np.fft.fftshift( np.fft.fft( X ) )[ N : ]
            imag    = np.imag(fft)
            real    = np.real(fft)
            s_avg   += np.sqrt( (real**2 + imag**2) / ( (N)*2) )
            temp_s  = np.sqrt( (real**2 + imag**2) / ( (N)*2) )
            fft_avg += fft

            N2      = len(y_temp2)/2 - 1
            fft2    = np.fft.fftshift( np.fft.fft( y_temp2 ) )[ N2 : ]
            imag2   = np.imag(fft2)
            real2   = np.real(fft2)
            s_avg2  += np.sqrt( (real2**2 + imag2**2) / ( (N2)*2) )
            temp_s2 = np.sqrt( (real2**2 + imag2**2) / ( N2 *2 ) )
            fft_avg2+= fft2

            count   += 1

        if index == 1:
            dat_c[i]= avg_y / 3.
            spe_c[i]= s_avg / 3.
            fft_c[i]= fft_avg / 3.

            ratio   = np.sum(spe_c[i]) / np.sum( s_avg2 / 3.)
            dat_c_s[i]= avg_y2 / 3.
            spe_c_s[i] = s_avg2 / 3. / ratio
            fft_c_s[i] = fft_avg2 / 3.

        if index == 2:
            dat_w[i]= avg_y / 3.
            spe_w[i]= s_avg / 3.
            fft_w[i]= fft_avg / 3.

            ratio   = np.sum(spe_w[i]) / np.sum( s_avg2 / 3.)
            dat_w_s[i]= avg_y2 / 3.
            spe_w_s[i]= s_avg2 / 3. / ratio
            fft_w_s[i]= fft_avg2 / 3.

# Contrast calculation
def cont_cal(spe_w, len_w, spe_c, len_c, f, N, len_T):
    S       = np.zeros( (len_c , len(f) ) )     # = Each contrast line
    Sf      = np.zeros( (len_T , len(f) ) )   # = Average contrast line per temperature point
    St      = 0
    sel     = [0,1]
    Nc      = 0

    for j in range(len_w):
        for k in sel:
            z   = j * 2 + k
            # print 'z: %-5s j: %s' % ( z, j)
            S[z]= ( spe_c[z] - spe_w[j] ) / spe_w[j]
            St  += S[z]
            Nc  += 1

            q       = j / N

            if Nc == N*2:
                q       = j / N
                Sf[q]   = St / float(N*2)
                St      = 0
                Nc      = 0

    return Sf, S

# Function used for cleaning 4D cube
def find_parameters(line, par):
    index   = line.index(str(par)) + len(par)
    cut     = line[index:index+10]
    return cut[:cut.index(";")]

# Function to select the closest point along a certain axis
def R_select(diff, axis, Zs):
    for i in range(len(axis)):
        if abs(axis[i] - Zs) <= diff:
            id_axis = i
            diff    = abs(axis[i] - Zs)
    return id_axis

'''
Step 1.1: Load data / Preliminaries
'''
T       = [5., 6., 7., 9., 11., 10., 10.8, 12., 13., 7., 9., 11., 13.]
start   = 854
end     = 1033
step    = 9
step2   = 3
N       = 4
meas1   = range(start, end+1)
temp1   = range(start+step2, end, step)
temp_w  = temp1 #+ temp2
temp_c  = []
for i in temp_w:
    temp_c.append(i-step2)
    temp_c.append(i+step2)
pos_step    = 20 * 10**(-6)
t           = pos_step / (3 * 10**(8) )
freqtep   = 1 / ( 152 * t )
f_s         = np.arange( 0, 152 ) * freqtep  * .5 * .5

'''
Step 1.2: Load data / Import measurment data
'''
# Data arrays
pos     = np.loadtxt('./dat/NbTiN200nm_'+str(start)+'.dat')[:,0]
dat_w   = np.zeros( ( len(temp_w) , len(pos) ) )
dat_c   = np.zeros( ( len(temp_c) , len(pos) ) )
f       = np.loadtxt('./fts/NbTiN200nm_'+str(start)+'.fts')[:,0]
spe_w   = np.zeros( ( len(temp_w) , len(f) ) )
spe_c   = np.zeros( ( len(temp_c) , len(f) ) )
fft_w   = np.zeros( ( len(temp_w) , len(f) ), dtype=complex )
fft_c   = np.zeros( ( len(temp_c) , len(f) ), dtype=complex )
pos_s   = pos[200:]
dat_w_s = np.zeros( ( len(temp_w) , len(pos_s) ) )
dat_c_s = np.zeros( ( len(temp_c) , len(pos_s) ) )
spe_w_s = np.zeros( ( len(temp_w) , 152 ) )
spe_c_s = np.zeros( ( len(temp_c) , 152 ) )
fft_w_s = np.zeros( ( len(temp_w) , 152 ), dtype=complex )
fft_c_s = np.zeros( ( len(temp_c) , 152 ), dtype=complex )

# Select data
selector(temp_c, 1, len(f) - 1 )
selector(temp_w, 2, len(f) - 1 )
Sf_s, S_s   = cont_cal(spe_w_s, len(temp_w), spe_c_s, len(temp_c), f_s, N, len(T) )

# Clean data
lines   = range(0,15) + range(65,79) + range(80,82) + range(84,85) + range(88,93) + range(95,102) + range(105,152)
y2      = np.delete(Sf_s[2], lines)
f_clean = np.delete(f_s, lines)

'''
Step 1.3: Load data / Import 4D data cube
'''
# Parameters for the import
i       = 0         # counter for frequency line
j       = 0         # counter for curve number
N_id    = 451       # total number of curves
N       = 1049      # total number of frequencies
f_fill  = True      # fill of the freq array enabled
count   = False     # counter for frequency enabled?
f_l     = 0        # Where to cut frequency such that starts at 300 GHz

# Arrays for the data
Rs_Xs   = np.zeros( ( N_id, 2 ) )
freq    = np.zeros( N )
R       = np.zeros( N )
Ra      = np.zeros( (N_id, 2 , N ) )

f       = open('nbtin-on-silicon-3thz.dat')     # load 4D data cube

line    = f.readline()                          # Read 4D cube line for line
while line:
    line    = f.readline()
    rows    = line.split('\t')

    if rows[0][0:2] == "Pa":
        Xs          = find_parameters(rows[0], "Xsheet=")
        Rs          = find_parameters(rows[0], "Rsheet=")
        Rs_Xs[j][0] = Rs
        Rs_Xs[j][1] = Xs

    if rows[0] == "100":                    # Starting frequency
        count   = True
    if count == True:
        # Calculating the power
        R[i]    = np.sqrt(float(rows[1])**2 + float(rows[2])**2)
        # frequency fill
        if f_fill == True :
            freq[i] = float(rows[0]) * 1e9
        # Setting counter to the next frequency
        i += 1
        if rows[0] == "3000":               # Ending frequency
            count   = False
            f_fill  = False
            # Storing frequency and R (power) in data cube Ra
            Ra[j][0]= freq
            Ra[j][1]= R
            #Changing counters
            i       = 0
            j       +=1
f.close()
x       = np.unique(Rs_Xs[:,0]) # Rs
y       = np.unique(Rs_Xs[:,1]) # Xs

'''
Step 2.1: Fitting / Preliminaries
'''
# Film parameters
ds          = .358E-6       #thickness of the film
T_Zs        = [4.6, 16]     # film temperatures: cold, warm
# Fitting parameters
Tc          = 15.65
sig_n       = 9.3E5         #6.7*10**(6)
fac_sign    = .86
tau         = 5 * 1e-15
delta       = 0.01

# Data storage
R_sim_MB    = np.zeros( ( len(T_Zs), len(freq)) )

'''
Step 2.2: Fitting / execute fitting procedure
'''
sig_film = []
for k in range(len(T_Zs)):
    if T_Zs[k] < Tc:
        Zs_mesh_MB, sig  = n.Zs(freq, T_Zs[k] , Tc, ds, 1, fac_sign * sig_n, tau, delta)
        sig_film.append( sig )
    else:
        Zs_mesh_MB, sig  = n.Zs(freq, T_Zs[k] , Tc, ds, 2, fac_sign * sig_n, tau, delta)
        sig_film.append( sig )

    for j in range(len(freq)):
        R_v         = Ra[:,1,j]                                     # j selects the frequency
        a           = np.lexsort((Rs_Xs[:,0], Rs_Xs[:,1]))
        R           = R_v[a].reshape((11,41))

        f_inter = interpolate.interp2d( x, y, R, kind='cubic')
        xnew    = np.arange(min(x), max(x), .01)
        ynew    = np.arange(min(y), max(y), .01)
        Rnew    = f_inter(xnew, ynew)

        Zs_MB_x , Zs_MB_y   = np.real(Zs_mesh_MB[j]), np.imag(Zs_mesh_MB[j])
        id_x_MB = R_select(1000, xnew, Zs_MB_x)
        id_y_MB = R_select(1000, ynew, Zs_MB_y)
        Rsel_MB = Rnew[id_y_MB, id_x_MB]
        R_sim_MB[k][j]  = Rnew[id_y_MB, id_x_MB]

'''
Step 3: Plotting and saving
'''
plt.rcParams.update({'font.size': 40, 'legend.fontsize': 30})
plt.rcParams['axes.linewidth'] = 4

from matplotlib.font_manager import FontProperties
font = FontProperties()

fig = plt.figure(1)
font.set_weight('bold')
fig.set_size_inches(20, 15)
# fit
plt.plot( freq/1e12, (R_sim_MB[0]-R_sim_MB[1])/R_sim_MB[1], linewidth=10, color="b", label="Model 4.6K Si")
# data
plt.plot( f_clean/1e12 , y2 ,color='b', marker=".", markersize=40, markerfacecolor='none',markeredgewidth=5, linestyle="", label="Data 4.6K Si")

plt.ylim((-0.1,.1))
plt.xticks(np.arange(0,3.000+.300,.300 ))
plt.xlim((0, 3.000))
# plt.xlabel("wavenumber (cm-1)")
plt.xlabel("Frequency (THz)")
plt.ylabel("Contrast reflection")
plt.legend(ncol=2)
# plt.grid(True)
plt.yticks(np.arange(-.075,.1+.025,.025 ))
plt.ylim((-.075,.1))
plt.title("$\\sigma=$"+str( fac_sign * sig_n /1e5)+"E5 S/m,Tc="+str(Tc)+"K, $\\delta=$"+str(delta)+", $\\tau=$"+str(tau/1e-15)+"fs")
# plt.savefig("./"+dir_name+"/nbtin_contrast_si_final_complex_sigma="+str( fac_sign * sig_n /1e5)+"e5Sm_Tc="+str(Tc)+'K_delta='+str(delta)+'_tau='+str(tau/1e-15)+"fs.png")
plt.show()

fit2 = (R_sim_MB[0]-R_sim_MB[1])/R_sim_MB[1]

# spec_file = open("./"+dir_name+"/contrast_fit_nbtin_si_paper.txt", 'w')
# spec_file.write('%-20s %s \n' % ("#Frequency", "Fit Modified MB" ))
# for i in range(len(freq)):
#     spec_file.write("%-20s %s \n" % (str(freq[i]/1e9), str(fit2[i])  ) )
# spec_file.close()

fig = plt.figure(2)
font.set_weight('bold')
fig.set_size_inches(20, 15)
models      = []
#plot points and lines
for j in range(len(T_Zs)):
    models.append(np.real(sig_film[j])/1e7)
    models.append(np.abs(np.imag(sig_film[j]))/1e7)
# real part fit
plt.plot(freq/1e12, models[0] , color="b", linewidth=10, label="Real 4.6K")
plt.plot(freq/1e12, models[2] , color="r", linewidth=10, label="Real 15.2K")
# imag part fit
plt.plot(freq/1e12, models[1] , color="b", linewidth=10, linestyle="--", label="Imag 4.6K")
plt.plot(freq/1e12, models[3] , color="r", linewidth=10, linestyle="--", label="Imag 15.2K")
plt.xticks(np.arange(0,3.000+.300,.300 ))
plt.xlim((.0, 3.000))
plt.ylim((-0.01,.4))
plt.xlabel("Frequency (THz)")
plt.ylabel("$\Omega$ (Sm/m x $10^7$)")
plt.legend(ncol=2)
plt.title("$\\sigma=$"+str( fac_sign * sig_n /1e5)+"E5 S/m,Tc="+str(Tc)+"K, $\\delta=$"+str(delta)+", $\\tau=$"+str(tau/1e-15)+"fs")
# plt.savefig("./"+dir_name+"/nbtin_conductivity_si_final_MB_complex_sigma="+str( fac_sign * sig_n /1e5)+"e5Sm_Tc="+str(Tc)+'K_delta='+str(delta)+'_tau='+str(tau/1e-15)+"fs.png")
plt.show()

# spec_file = open('./'+dir_name+'/conductivity_fit_nbtin_si_paper.txt', 'w')
# spec_file.write('%-20s %-20s %-20s %-20s %s \n' % ("#Frequency", "Real 6K ", "Imag 6K ", "Real 15.1k ", "Imag 15.1k " ))
# for i in range(len(freq)):
#     spec_file.write("%-20s %-20s %-20s %-20s %s \n" % (str(freq[i]/1e9), str(models[0][i]), str(models[1][i]), str(models[2][i]), str(models[3][i]) ) )
# spec_file.close()
