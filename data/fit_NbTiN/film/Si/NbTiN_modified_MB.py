#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cmath as cm	#necessary to solve expressions like sqrt(complex_value)
import compex_integration as ci
from scipy import integrate
import matplotlib.pyplot as plt

# Constants
tolarance = 1*10**(-25)
ee 		= 1.60217733 *10**(-19)
kb 		= 1.380658*10**(-23)/ee
hh 		= 6.6260755 *10**(-34)/ee
T  		= 5.5

sig_a 	= 9E5
mu_0 	= 1.257 *10**(-6)
eps_0 	= 8.8542 *10**(-12)
c_0 	= 2.9979 *10**(8)
a 		= .000001
m_e 	= 9.10938356 *10**(-31)
Vf 		= 1.37 *10**(6)
n 		= 5.56 *10**(28)

Zspace 	= np.sqrt( mu_0 / eps_0 )

# Formulas
def Delta_a(T , Tc, delta):
	if T > Tc :
		return 0
	elif T <= Tc :
		D1 = 1.9 * kb * Tc * np.tanh( 1.74 * np.sqrt( Tc / T - 1 ) )
		D2 = D1 * delta
		D0 = complex(D1,D2)
		return D0

# f
def f( E, T):
	x 	=  np.exp( ( E / ( kb * T ) ) )
	return 1 / ( 1 + x )

# Function that handles a1 and a2 integration
def a1_a2(F, Tc, T, Delta, num):
	a_val = np.zeros( (len( F ) ), dtype = complex  )

	# Upper and lower bound of integration
	if num == '1':
		lb = Delta * (1 + a*0)
		ub = 3 * Delta
	elif num == '2':
		lb = 3 * Delta
		ub = 60 * Delta

	# Actual integration
	for i in range(len(F)):
		res = ci.c_integr( int_a1_a2 , np.real(lb) , np.real(ub) , args=(F[i], T, Delta)) #,tol=tolarance, divmax=20, epsabs=tolarance, limit=100 )
		a_val[i] = res
	return a_val

# a1 & a2 that needs to be integrated
def int_a1_a2(e, freq, T, Delta):
	f1 	= f( e, T)
	f2 	= f( e + hh * freq, T)

	num = ( e**2 + Delta**2 + hh * freq * e)
	dnum= cm.sqrt( (e**2 - Delta**2) * ( (e + hh * freq)**2 - Delta**2 ) )

	if dnum == 0.0 :
		return 0
	else:
		return (f1 - f2) * num / dnum

# Function that handles the a3 integration
def a3(F, Tc, T, Delta):
	a_val = np.zeros( (len( F ) ), dtype = complex )
	ub = np.zeros( (len( F )) )

	# Upper and lower bound of integration
	lb = Delta * (1 + a)

	# Actual integration
	for i in range(len(F)):
		ub[i] = hh * F[i] - (np.real(Delta)) * (1.0 + 0*a)
		if hh * F[i] >= 2 * np.real(Delta):
			a_val[i] = ci.c_integr( int_a3 , np.real(lb), np.real(ub[i]), args=(F[i], T, Delta)) #,tol=tolarance, divmax=20, , epsabs=tolarance, limit=100  )
		else:
			a_val[i] = 0

	return a_val

# a3 function that needs to be integrated
def int_a3(e, freq, T, Delta):

	f1 	= f(hh * freq - e , T)

	num = hh * freq * e - e**2 - Delta**2
	d 	= (e**2 - Delta**2 ) * ( ( hh * freq - e )**2 - Delta**2 )

	dnum = cm.sqrt(d)
	if dnum == 0.0:
		return 0.0
	else:
		return (1 - 2 * f1) * num / dnum

# Function that handles the a4 integration
def a4(F, Tc, T, Delta):
	a_val 	= np.zeros( (len( F ) ) , dtype=complex)
	a_4 	= np.zeros(2, dtype=complex)
	a_4[0]	= -1 * Delta * (1 + a)

	# Upper and lower bound of integration
	ub = Delta * (1 + a)

	# Actual integration
	for i in range(len(F)):
		a_4[1]	= Delta * (1 + a) - hh * F[i]
		a_val[i] =ci.c_integr( int_a4 , np.max(np.real(a_4)), np.real(ub), args=(F[i], T, Delta)) #,tol=tolarance, divmax=20  epsabs=tolarance, limit=100 )

	return a_val

# a3 function that needs to be integrated
def int_a4(e, freq, T, Delta):
	f1 	= f(hh * freq + e , T)
	num 	= hh * freq * e + e**2 + Delta**2
	d 		=  ( (-e**2 + Delta**2 ) * ( ( hh * freq + e )**2 - Delta**2 ) )
	dnum 	= cm.sqrt( d )
	if dnum == 0.0:
		return 0.0
	else:
		return (1 - 2 * f1) * num / dnum

# Function that handles the complex part of the integration
def a5(F, Tc, T, Delta, D2):
	a_val 	= np.zeros( (len( F ) ), dtype = complex )

	# Actual integration
	for i in range(len(F)):
		if D2 == 0:
			a_val[i] = 0
		else:
			a_val[i] = ci.c_integr( int_a5 , D2 - D2 * 1e-8, 0 , args=(F[i], T, Delta)) #,tol=tolarance, divmax=20, epsabs=tolarance, limit=100 )

	return a_val

# a5 function that needs to be integrated
def int_a5(epsilon, freq, T, Delta):
	E 	= np.real(Delta) + 1j * epsilon

	f1 	= f(hh * freq + E , T)

	num2	= E * ( E - hh * freq ) + Delta**2
	num3	= E * ( E + hh * freq ) + Delta**2

	dnum1 	= np.sqrt( E**2 - Delta **2 )
	dnum2 	= np.sqrt( ( E - hh * freq )**2 - Delta**2 )
	dnum3 	= np.sqrt( ( E + hh * freq )**2 - Delta**2 )

	return (1 - 2 * f1) / dnum1 * ( num2 / dnum2 - num3 / dnum3 )

# Function for calculating conductivity
def sig(F, T, Tc, delta):
	if T > Tc:
		a_1 = a1_a2( F, Tc, T, 0. , '1' )
		a_2 = a1_a2( F, Tc, T, 0. , '2' )
		a_3 = a3( F, Tc, T,    0. )
		a_4 = a4( F, Tc, T,    0. )
		a_5 = a4( F, Tc, T,    0. )

	elif T <= Tc:
		Delta 	= Delta_a(T , Tc, delta)
		D2 = np.imag(Delta)
		a_1 = a1_a2( F, Tc, T, Delta , '1' )
		a_2 = a1_a2( F, Tc, T, Delta , '2' )
		a_3 = a3( F, Tc, T, Delta )
		a_4 = a4( F, Tc, T, Delta )
		a_5 = a5( F, Tc, T, Delta, D2 )
	sigma = []
	for i in range(len(F)):
		sig_1 = (a_1[i] + a_2[i])*2. / (hh * F[i]) + a_3[i]* 1. / (hh * F[i])
		sig_2 = a_4[i] * 1. / (hh * F[i])
		sigma.append(sig_1 - 1j * sig_2 + a_5[i] * 1. / (hh * F[i]))

	return sigma

# Function for calculating Zs
def Zs(F, T, Tc, ds, val, sig_n, tau, delta):
	if val == 1:
		sig_d 	= sig_n *( 1 / (1 -  1j * 2 * np.pi * F * tau ) )
		dnum 	= sig(F, T, Tc, delta) * sig_d

	elif val == 2:
		sig_d 	= sig_n *( 1 / (1 -  1j * 2 * np.pi * F * tau ) )
		dnum 	= sig_d

	num 	= 2 * 1j * np.pi * F * mu_0
	y 		= np.sqrt( num / dnum )
	x 		= np.sqrt( num * dnum ) * ds
	coth 	= ( np.exp( x ) + np.exp( -1 * x ) ) / ( np.exp( x ) - np.exp( -1 * x ) )
	Zs 		= y * coth

	return Zs , dnum

# Function for calculating R
def R(F, T, Tc, ds, val, sig_n, tau, delta):
	Z_s, sig_calc 	= Zs(F, T, Tc, ds, val, sig_n, tau, delta)
	R 				= (Z_s - Zspace) / (Z_s + Zspace)

	return R, sig_calc
