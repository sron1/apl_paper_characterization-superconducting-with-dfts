NbTiN Si contrast data was fitted using modified Mattis Bardeen theory (NbTiN_modified_MB.py) via a python script (fit_NbTiN_si_film.py).

Input:
- .dat files: interferograms
- .fts files: spectra
- nbtin-on-silicon-3thz.dat: 4D data array relating frequency, Rs, Xs and R

Inputs were used to visually fit contrast data, where alpha (via Tc), conductivity, tau and delta were used as fitting parameters. 
A simple spline interpolation was applied to R values ~1.25 - 1.70 THz, since the 4D cube produced by CST Microwave Studio 2016
contained an numerical, but unphysical, error.

Output:
- contrast_data_nbtin_si_paper.txt 		: contrast data
- contrast_fit_nbtin_si_paper.txt		: final contrast fit 
- contrast_NbTiN_mesh_new.dat 			: Spline corrected contrast curve (ie spline applied to contrast_fit_nbtin_si_paper.txt)
- conductivity_fit_nbtin_si_paper.txt 	: final conductivity fit
