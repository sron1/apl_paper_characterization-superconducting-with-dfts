NbTiN Quartz (Qu) contrast data was fitted using modified Mattis Bardeen theory (NbTiN_modified_MB.py) via a python script (fit_NbTiN_qu_film.py).

Input:
- .dat files: interferograms
- .fts files: spectra
- nbtin-on-quartz-3thz.dat: 4D data array relating frequency, Rs, Xs and R

Inputs were used to visually fit contrast data, where alpha (via Tc), conductivity, tau and delta were used as fitting parameters. 

Output:
- contrast_data_nbtin_qu_paper.txt 		: contrast data
- contrast_fit_nbtin_qu_paper.txt		: final contrast fit 
- conductivity_fit_nbtin_qu_paper.txt 	: final conductivity fit
