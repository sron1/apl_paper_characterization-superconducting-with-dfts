NbTiN Si solid contrast data was fitted using Mattis Bardeen theory (NbTiN_MB.py) via a python script (fit_NbTiN_si_solid.py).

Input:
- NbTiN_solid_Si_good.txt	: 	contains simulated frequency range
- BUF1.txt					:   contains measurement frequency range
- BUF2.txt 					:   contains contrast curve

Inputs were used to visually fit contrast data, where alpha (via Tc), conductivity, tau and delta were used as fitting parameters. 

Output:
- contrast_data_nbtin_solid_si_paper.txt 											: contrast data
- contrast_fit_nbtin_solid_si_paper.txt												: final contrast fit
- nbtin_contrast_solid_si_paper_sigma=56.95_Tc=15.825K_tau=10.0fs_delta=0.05.png 	: image with final contrast fit

NOTE: the contrast curve for NbTiN Si and Quartz were idententical, therefore this procedure was NOT repeated for NbTiN on Quartz
