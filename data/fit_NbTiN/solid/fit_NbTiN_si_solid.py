#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
This python script describes the fitting of the constrast curve of the NbTiN Si solid film by Mattis Bardeen (MB) theory.

The outline of the script is as follows:
Step 0: Preliminaries
    - Load packages
    - define functions

Step 1: Load data
    - Preliminaries
    - load measurment data

Step 2: fitting and storage
    - Preliminaries
    - execute fitting procedure

Step 3: Plotting and saving
'''

'''
Step 0.1: Preliminaries / load packages
'''
# Plotting packages
import matplotlib.pyplot as plt
import matplotlib.collections as mcol
import matplotlib.transforms as mtransforms
from matplotlib.legend_handler import HandlerPathCollection
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

# Numerical packages
import numpy as np
from scipy import optimize
from scipy import interpolate

# Mattis Bardeen package
import NbTiN_MB as n

'''
Step 0.2: Preliminaries / Functions
'''
#Fomulas
class HandlerMultiPathCollection(HandlerPathCollection):
    """
    Handler for PathCollections, which are used by scatter
    """
    def create_collection(self, orig_handle, sizes, offsets, transOffset):
        p = type(orig_handle)(orig_handle.get_paths(), sizes=sizes,
                              offsets=offsets,
                              transOffset=transOffset,
                              )
        return p

'''
Step 1.1: Load data / Preliminaries
'''
## load data freq and contrast data
lines2      = range(0,30) + range(98,112) + range(112,115)
lines       = range(0,15)
'''
Step 1.2: Load data / Import measurment data
'''
f_s         = np.loadtxt('./NbTiN_solid_Si_good.txt')[:,0] * 1e9
Sf_s        = np.loadtxt('./NbTiN_solid_Si_good.txt')[:,1]
f_s2        = np.loadtxt('./BUF1.txt')
Sf_s2       = np.loadtxt('./BUF2.txt')
# Clean data
f_s2_clean  = np.delete(f_s2, lines2)
Sf_s2_clean = np.delete(Sf_s2, lines2)
y0      = np.delete(Sf_s, lines)
f_clean = np.delete(f_s, lines)
f_s     = f_clean

'''
Step 2.1: Fitting / Preliminaries
'''
# Film parameters
ds      = .2000E-6      # thickness of the film
T_Zs    = [ 5.5 , 10 ]  # film temperature: cold, warm
# Fitting parameters
Tc      = 9.1           #Tc
sig_n   = 6.7E6 / 6     # scaled conductivity to match contrast
tau     = 1e-15
delta   = .05

# Data storage
R_sim_MB   = np.zeros( ( len(T_Zs), len(f_s)) )

'''
Step 2.2: Fitting / execute fitting procedure
'''
sig_film = []
for k in range(len(T_Zs)):
    print T_Zs[k]

    if T_Zs[k] < Tc:
        R_calc, sig     = n.R(f_s, T_Zs[k] , Tc, ds, 1, sig_n, tau, delta)
        sig_film.append( sig )
    else:
        R_calc, sig     = n.R(f_s, T_Zs[k] , Tc, ds, 2, sig_n, tau, delta)
        sig_film.append( sig )

    for j in range(len(f_s)):
        R_sim_MB[k]  = R_calc

'''
Step 3: Plotting and saving
'''
plt.rcParams.update({'font.size': 40, 'legend.fontsize': 30})
plt.rcParams['axes.linewidth'] = 4

from matplotlib.font_manager import FontProperties
font = FontProperties()

fig = plt.figure(1)
font.set_weight('bold')
fig.set_size_inches(15, 10)
plt.title( "$\sigma="+str(round(fac_sign_MB * sig_n/1e6,1))+"S/m, T_c=$"+str(Tc)+"K, $\\tau=$"+str(fac_tau_MB/1e-15)+"fs"+",$\\delta$="+str(delta) )
# measured contrast
plt.plot(f_s2_clean / 1e3, 100*Sf_s2_clean, color='b', marker=".", markersize=5, markerfacecolor='none',markeredgewidth=5, linestyle="", label="Si Data 5.5K ")
# fit
plt.plot(f_s/1e12, 100*(R_sim_MB[0]-R_sim_MB[1])/R_sim_MB[1], linewidth=5, color="b",label="Si Model 5.5K")
plt.xticks(np.arange(0,3.000+.300,.300 ))
plt.xlim((.0, 3.000))
# plt.xlabel("wavenumber (cm-1)")
plt.xlabel("Frequency (THz)")
plt.ylabel("Contrast reflection (%)")
plt.legend(ncol=2)
# plt.grid(True)
plt.yticks(np.arange(-5,.5+5,.5 ))
plt.ylim((-5,5))
plt.grid(True)

# plt.savefig("./complex_and_drude_test_Nb_solid/nbtin_contrast_final_solid_si_paper_sigma="+str(fac_sign_MB * sig_n/1e5)+"_Tc="+str(Tc)+"K_tau="+str(fac_tau_MB/1e-15)+"fs"+"_delta="+str(delta)+".png")
plt.show()

contrast = 100*(R_sim_MB[0]-R_sim_MB[1])/R_sim_MB[1]

# Save fit data
F = open("./complex_and_drude_test_Nb_solid/contrast_fit_nbtin_solid_si_paper.txt", "w")
F.write('%-20s %s\n' % ( "#Frequency", "Contrast") )
for j in range(len(f_s)):
    F.write('%-20s %s\n' % ( str(f_s[j]/1e9), str( contrast[j] ) ) )
F.close()
# Save contrast data
F = open("./complex_and_drude_test_Nb_solid/contrast_data_nbtin_solid_si_paper.txt", "w")
F.write('%-20s %s\n' % ( "#Frequency", "Contrast") )
for j in range(len(f_s2_clean)):
    F.write('%-20s %s\n' % ( str(f_s2_clean[j]/1e3), str( 100*Sf_s2_clean[j] ) ) )
F.close()
