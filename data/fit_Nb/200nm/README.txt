Same fitting procedure was followed for Nb200nm film as for 70nm. Here, the main interest was the reduction on contrast, 
due an increase in thickness.

Due to change in computer the used python script was lost and could not be recovered. Also, the underlying input files (i.e. .dat and .fts files)
were lost. This was deemed not essential, since the 200nm was primarly used for verifying the drop in contrast, while the fit parameters from the
70nm Nb film were used.

Fortunately, the output files were backed up properly:
- freq_s_200nm.npy		: frequency range from measurement data
- f_clean_200nm.npy 	: selected frequency range used for fit 
- R_4k_200nm.npy 		: measured contrast curve 4K
- R_4k_200nm.npy 		: measured contrast curve 6K
- R_sim_200nm.npy 		: simulated contrast curves
- Con_200nm.npy 		: simulated conductivity curves
