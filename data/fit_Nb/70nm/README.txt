Nb contrast data was fitted using modified Mattis Bardeen theory (Nb_modified_MB.py) via a python script (fit_Nb75nm.py).

Input:
- .dat files: interferograms
- .fts files: spectra
- nb-on-si-18-3.3.dat: 4D data array relating frequency, Rs, Xs and R

Inputs were used to visually fit contrast data, where alpha (via Tc), conductivity, tau and delta were used as fitting parameters. 

Output:
- contrast_data_nb70_si_complex_Tc=8.95K_delta=0.01_tau=6.0fs.txt 		: contrast data
- contrast_fit_nb70.txt													: final contrast fit
- contrast_nb70_si_final_complex_Tc=8.95K_delta=0.01_tau=6.0fs.png 		: image with final contrast fit
- conductivity_fit_nb70.txt 											: final conductivity fit
- conductivity_nb70_si_final_complex_Tc=8.95K_delta=0.01_tau=6.0fs.png 	: image with final conductivity fit
