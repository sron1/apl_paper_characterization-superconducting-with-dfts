#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
This python script describes the fitting of the constrast curve of the Nb film by modified Mattis Bardeen (MB) theory.

The outline of the script is as follows:
Step 0: Preliminaries
    - Load packages
    - define functions

Step 1: Load data
    - Preliminaries
    - load measurment data
    - load 4D data cube

Step 2: fitting and storage
    - Preliminaries
    - execute fitting procedure

Step 3: Plotting and saving
'''

'''
Step 0.1: Preliminaries / load packages
'''
# Plotting packages
import matplotlib.pyplot as plt
import matplotlib.collections as mcol
import matplotlib.transforms as mtransforms
from matplotlib.legend_handler import HandlerPathCollection
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

# Numeric packages
import numpy as np
from scipy import optimize
from scipy import interpolate

# Modified Mattis-Bardeen (MB) package
import Nb_modified_MB as n

# Set directory
dir_name    = 'complex3' ;

'''
Step 0.2: Preliminaries / Functions
'''
## Data selector function
def selector(temp, index):
    for i in range(len(temp)):
        count   = 0
        avg_y   = 0
        s_avgcom= 0
        s_avg   = 0

        while count < 3 :
            y       = np.loadtxt('./dat/Nb75nm_'+str( temp[i] + count )+'.dat')[:,1]
            a, b    = np.polyfit(pos, y, 1)
            y_new   = a * pos + b
            avg_y   += y - y_new
            s       = np.loadtxt('./fts/Nb75nm_'+str( temp[i] + count )+'.fts')[:,1]
            s_avg   += s
            count   += 1

        if index == 1:
            dat_c[i]= avg_y / 3.
            spe_c[i]= s_avg / 3.

        if index == 2:
            dat_w[i]= avg_y / 3.
            spe_w[i]= s_avg / 3.

# Constrast colculation function
def cont_cal(spe_w, len_w, spe_c, len_c, f, N):
    S       = np.zeros( (len_c , len(f) ) )     # = Each contrast line
    Sf      = np.zeros( (4 , len(f) ) )         # = Average contrast line per temperature point
    St      = 0
    Sl      = 58
    Sr      = 72
    N_a     = 100000
    a       = np.linspace(0.0, 1.1, N_a )
    sel     = [0,1]
    Nc      = 0
    count   = 0

    for j in range(len_w):
        Sw = spe_w[j][Sl:Sr]
        for k in sel:
            Sarr = []
            z   = j * 2 + k
            Sc  = spe_c[z][Sl:Sr]
            S[z]= ( spe_c[z] - spe_w[j] ) / spe_c[j]
            St  += S[z]
            Nc  += 1
            if Nc == N*2:
                q       = j / N
                Sf[q]   = St / float(N*2)
                St      = 0
                Nc      = 0
    return Sf, S

# Function to remove issues from data
def find_parameters(line, par):
    index   = line.index(str(par)) + len(par)
    cut     = line[index:index+20]
    return cut[:cut.index(";")]

# Function to select the closest point along a certain axis
def R_select(diff, axis, Zs):
    for i in range(len(axis)):
        if abs(axis[i] - Zs) <= diff:
            id_axis = i
            diff    = abs(axis[i] - Zs)
    return id_axis

'''
Step 1.1: Load data / Preliminaries
'''
# Preliminaries for data selection
T       = [4.7, 5., 6., 7.2 ]
start0  = 666
end0    = 701
start   = 738
end     = 845
step    = 9
step2   = 3
N       = 4
meas_all= range(start, end+1)
temp_w  = range(start+step2, end, step) + range(start0+step2, end0, step)
temp_c  = []
for i in temp_w:
    temp_c.append(i-step2)
    temp_c.append(i+step2)
# measurement data arrays
pos     = np.loadtxt('./dat/Nb75nm_'+str(start)+'.dat')[:,0]
dat_w   = np.zeros( ( len(temp_w) , len(pos) ) )
dat_c   = np.zeros( ( len(temp_c) , len(pos) ) )
f       = np.loadtxt('./fts/Nb75nm_'+str(start)+'.fts')[:,0]
spe_w   = np.zeros( ( len(temp_w) , len(f) ) )
spe_c   = np.zeros( ( len(temp_c) , len(f) ) )

'''
Step 1.2: Load data / Import measurment data
'''
# Load  measured interferogram, spectra and deteremine calculate contrast
selector(temp_c, 1)
selector(temp_w, 2)
Sf, S   = cont_cal(spe_w, len(temp_w), spe_c, len(temp_c), f, N )
# Write measured contrast data to file to make plotting (later on) easier
spec_file = open('spec_w_c_nb.txt', 'w')
spec_file.write('%-20s %-20s %s \n' % ("#Frequency", "Cold spectrum ", "Warm spectrum" ) )
for i in range(len(spe_c[0])):
    spec_file.write("%-20s %-20s %s \n" % (str(f[i]/1e9), str(spe_c[0][i]), str(spe_w[0][i]) ) )
spec_file.close()

# Selecting data to use for fitting
id_b    = 25
id_e    = 108
x1      = f[id_b:id_e]
lines   = [49, 53,]+range(69,83)
y0      = np.delete(Sf[0][id_b:id_e], lines)
y2      = np.delete(Sf[2][id_b:id_e], lines)
f_clean = np.delete(x1, lines)

'''
Step 1.3: Load data / Import 4D data cube
'''
# Parameters for the import
i       = 0         # counter for frequency line
j       = 0         # counter for curve number
N_id    = 451       # total number of curves
N       = 1049      # total number of frequencies
f_fill  = True      # fill of the freq array enabled
count   = False     # counter for frequency enabled?

# Arrays for the data
Rs_Xs   = np.zeros( ( N_id, 2 ) )
freq    = np.zeros( N )
R       = np.zeros( N )
Ra      = np.zeros( (N_id, 2 , N ) )

# Load 4D data array
f       = open('nb-on-si-18-3.3.dat')       # Reading the 4D data cube
line    = f.readline()                      # Readling line for line
while line:
    line    = f.readline()
    rows    = line.split('\t')

    if rows[0][0:2] == "Pa":
        Xs          = find_parameters(rows[0], "Xsheet=")
        Rs          = find_parameters(rows[0], "Rsheet=")
        Rs_Xs[j][0] = Rs
        Rs_Xs[j][1] = Xs

    if rows[0] == "100":                    # Starting frequency
        count   = True
    if count == True:
        # Calculating the power
        R[i]    = np.sqrt(float(rows[1])**2 + float(rows[2])**2)
        # frequency fill
        if f_fill == True :
            freq[i] = float(rows[0]) * 1e9
        i += 1
        if rows[0] == "2000":               # Ending frequency
            count   = False
            f_fill  = False
            # Storing frequency and R (power) in data cube Ra
            Ra[j][0]= freq
            Ra[j][1]= R
            i       = 0
            j       +=1
f.close()
x       = np.unique(Rs_Xs[:,0]) # Rs
y       = np.unique(Rs_Xs[:,1]) # Xs

'''
Step 2.1: Fitting / Preliminaries
'''
# Film parameters
ds      = .07E-6            # thickness of the film
T_Zs    = [4.7, 6.0 , 10.]  # Temperatures film: cold 1, cold 2, warm in Kelvin
# Fitting parameters
sig_n   = 6.7E6             # conductivity
fac_sign= 2.2               # scaling factor conductivity
tau     = 6*1e-15
Tc      = 8.95              # critical temperature
delta   = .01

# Arrays for reflection coefficient, R
R_sim   = np.zeros( ( len(T_Zs), len(freq)) )
R_sim_MB= np.zeros( ( len(T_Zs), len(freq)) )
Rs_MB   = np.zeros( ( len(freq), len(T_Zs)+1 ) )
Xs_MB   = np.zeros( ( len(freq), len(T_Zs)+1 ) )
Rs_MB[:,0] = freq/1e12
Xs_MB[:,0] = freq/1e12

'''
Step 2.2: Fitting / execute fitting procedure
'''
sig_film = []
for k in range(len(T_Zs)):
    if T_Zs[k] < Tc:
        Zs_mesh_MB, sig     = n.Zs(freq, T_Zs[k] , Tc, ds, 1, fac_sign * sig_n, tau, delta)
        sig_film.append( sig )
    else:
        Zs_mesh_MB, sig     = n.Zs(freq, T_Zs[k] , Tc, ds, 2, fac_sign * sig_n, tau, delta)
        sig_film.append( sig )

    for j in range(len(freq)):
        R_v         = Ra[:,1,j]
        a           = np.lexsort((Rs_Xs[:,0], Rs_Xs[:,1]))
        R           = R_v[a].reshape((11,41))
        f_inter = interpolate.interp2d( x, y, R, kind='cubic')
        xnew    = np.arange(min(x), max(x), .01)
        ynew    = np.arange(min(y), max(y), .01)
        Rnew    = f_inter(xnew, ynew)
        Zs_MB_x , Zs_MB_y   = np.real(Zs_mesh_MB[j]), np.imag(Zs_mesh_MB[j])
        id_x_MB = R_select(1000, xnew, Zs_MB_x)
        id_y_MB = R_select(1000, ynew, Zs_MB_y)
        Rsel_MB = Rnew[id_y_MB, id_x_MB]
        R_sim[k][j]     = Rnew[id_y_MB, id_x_MB]

'''
Step 3: Plotting and saving
'''
plt.rcParams.update({'font.size': 40, 'legend.fontsize': 30})
plt.rcParams['axes.linewidth'] = 4
from matplotlib.font_manager import FontProperties
font = FontProperties()

# Plot contrast curves
fig = plt.figure(1)
font.set_weight('bold')
fig.set_size_inches(22, 15)
plt.grid(True, linewidth=3)

plt.title("$\\sigma=$"+str( fac_sign * sig_n /1e6)+",Tc="+str(Tc)+"K, $\\delta=$"+str(delta)+", $\\tau=$"+str(fac_tau/1e-15)+"fs")
# measured data
plt.plot( freq/1e12, 100*(R_sim[0]-R_sim[2])/R_sim[2], linewidth=10, color="b", label="MB 4.7K ")
plt.plot( freq/1e12, 100*(R_sim[1]-R_sim[2])/R_sim[2], linewidth=10, color="r", label="Model 6.0K ")
# fit
plt.plot( f_clean/1e12 , 100*y0  ,color='b', marker=".", markersize=40, markerfacecolor='none',markeredgewidth=5, linestyle="", label="Data 4.7K ")
plt.plot( f_clean/1e12 , 100*y2  ,color='r', marker=".", markersize=40, markerfacecolor='none',markeredgewidth=5, linestyle="", label="Data 6.0K ")

plt.xticks(np.arange(0,1.800+.300,.300 ))
plt.xlim((0, 1.800))
plt.xlabel("Frequency (THz)")
plt.ylabel("Contrast reflection ( % )")
plt.ylim((-3,3))
plt.legend(ncol=2)
# plt.savefig("./"+dir_name+"/contrast_nb70_si_final_complex_Tc="+str(Tc)+"K_delta="+str(delta)+"_tau="+str(fac_tau/1e-15)+"fs.png")
plt.show()

# Store contrast data
con1= 100*(R_sim[0]-R_sim[2])/R_sim[2]
con2= 100*(R_sim[1]-R_sim[2])/R_sim[2]
F   = open("./"+dir_name+"/contrast_data_nb70_si_complex_Tc="+str(Tc)+"K_delta="+str(delta)+"_tau="+str(fac_tau/1e-15)+"fs.txt", "w")
F.write('%-20s %-20s %s\n' % ( "#Frequency Data", "Data 4.7K", "Data 6K") )
for j in range(len(f_clean)):
    F.write('%-20s %-20s %s\n' % ( str(f_clean[j]/1e12), str( y0[j] ), str( y2[j] ) ) )
F.close()
F   = open("./"+dir_name+"/contrast_fit_nb70.txt", "w")
F.write('%-20s %-20s %s\n' % ( "#Frequency Fit", "Fit 4.7K", "Fit 6K") )
for j in range(len(freq)):
    F.write('%-20s %-20s %s\n' % ( str(freq[j]/1e12), str( con1[j] ), str( con2[j] ) ) )
F.close()

# plot conductivities
models      = []
# Plot points and lines
for j in range(len(T_Zs)):
    models.append(np.real(sig_film[j])/1e8)
    models.append(np.abs(np.imag(sig_film[j]))/1e8)

fig = plt.figure(1)
font.set_weight('bold')
fig.set_size_inches(20, 15)
# real part fit
plt.plot(freq/1e12, models[0] , color="b", linewidth=10, label="Real 4.7K")
plt.plot(freq/1e12, models[2] , color="r", linewidth=10, label="Real 6.0K")
plt.plot(freq/1e12, models[4] , color="y", linewidth=10, label="Real 10.K")
# imaginary part fit
plt.plot(freq/1e12, models[1] , color="b", linewidth=10, linestyle="--", label="Imag 4.6K")
plt.plot(freq/1e12, models[3] , color="r", linewidth=10, linestyle="--", label="Imag 6.0K")
plt.plot(freq/1e12, models[5] , color="y", linewidth=10, linestyle="--", label="Imag 10.K")

plt.xticks(np.arange(0,1.8+.3,.3 ))
plt.xlim((.0, 1.8))
plt.ylim((-0.1,1.25))
plt.xlabel("Frequency (THz)")
plt.ylabel("$\Omega$ (Sm/m x $10^7$)")
plt.legend(ncol=2)
plt.title("$\\sigma=$"+str( fac_sign * sig_n /1e6)+",Tc="+str(Tc)+"K, $\\delta=.0005$, $\\tau=$"+str(fac_tau/1e-15)+"fs")
# plt.savefig("./"+dir_name+"/conductivity_nb70_si_final_complex_Tc="+str(Tc)+"K_delta="+str(delta)+"_tau="+str(fac_tau/1e-15)+"fs.png")
plt.show()

# Store conductivities
F = open("./"+dir_name+"/conductivity_fit_nb70.txt", "w")
F.write('%-20s %-20s %-20s %-20s %-20s %-20s %s\n' % ( "#Frequency", "Real 4.7K", "Imag 4.7K", "Real 6K", "Imag 6K", "Real 10K", "Imag 10K") )
for j in range(len(freq)):
    F.write('%-20s %-20s %-20s %-20s %-20s %-20s %s\n' % ( str(freq[j]/1e9), str( models[0][j] ), str( models[1][j] ), str( models[2][j] ), str( models[3][j] ), str( models[4][j] ), str( models[5][j] ) ) )
F.close()
